<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PHOTO</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

</head>



<body background="img/GettyImages-1181916538-728x485.jpg">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="TopPageServlet">UFC</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="NewsServlet">NEWS</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="RankingServlet" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            RANKING
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                    </li>
                     <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                </ul>

            </div>
        </nav>

        <h2 class="text-light">PHOTO</h2>
        <div class="row">
	<c:forEach var="pdbList" items="${pdb}">

            <div class="col-4">
                <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
            </div>

	   </c:forEach>
	    </div>


      <!--     <div class="row">
            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>
        </div>


          <div class="row">
            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass} width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/${pdbList.file_pass}" width="100%">
                <p style="color: azure">${pdbList.file_comment}</p>
                </div>
            </div>
        </div>
        -->

    </div>
  <!--    <footer>
        <nav aria-label="...">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="前">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link" href="photo.html">1</a></li>
                <li class="page-item"><a class="page-link" href="photo2.html">2</a></li>
                <li class="page-item"><a class="page-link" href="photo3.html">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="photo2.html" aria-label="次">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>


    </footer>  -->

</body>

</html>
