<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>middleWeight</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

</head>

<body class="a" background="img/GettyImages-1181916538-728x485.jpg">
    <div class="container">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="TopPageServlet">UFC</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="NewsServlet">NEWS</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="RankingServlet" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                RANKING
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                        </li>
                         <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                    </ul>

                </div>
            </nav>
        </header>

        <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">RANKING</th>
                    <th scope="col">NAME</th>
                    <th scope="col">ミドル級</th>
                </tr>
            </thead>
             <c:forEach var="fdb" items="${fdbList}">
            <tbody>
                <tr>
                    <th scope="row">${fdb.rank}</th>
                    <td><a href="FighterDetailServlet?id=${fdb.id}" class="text-light">${fdb.name}</a></td>
                    <td class=”table-img”>
                        <img src="img/${fdb.file_pass}"  width="100%" height="400px">
                    </td>
                </tr>
              </c:forEach>

          <!--        <tr>
                    <th scope="row">1</th>
                    <td><a href="fighterdetail.html" class="text-light">ロバート・ウィテカー</a></td>
                    <td class=”table-img”>
                        <img src="img/fighter_img(31).jfif">
                    </td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="fighterdetail.html" class="text-light">ヨエル・ロメロ</a></td>
                    <td class=”table-img”>
                        <img src="img/fighter_img(32).jfif" width="90%">
                    </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="fighterdetail.html" class="text-light">ジャレッド・キャノニア</a></td>
                    <td class=”table-img”>
                        <img src="img/fighter_img(33).jpg">
                    </td>

                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td><a href="fighterdetail.html" class="text-light">ダレン・ティル</a></td>
                    <td class=”table-img”>
                        <img src="img/fighter_img(34).jfif">
                    </td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td><a href="fighterdetail.html" class="text-light">ジャック・ハーマンソン</a></td>
                    <td class=”table-img”>
                        <img src="img/fighter_img(35).jpg">
                    </td>
                </tr>
            </tbody>
            -->
        </table>

    </div>
</body>

</html>
