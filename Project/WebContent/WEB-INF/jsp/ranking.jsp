<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">
</head>

<body background="img/GettyImages-1181916538-728x485.jpg">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="TopPageServlet">UFC</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="NewsServlet">NEWS</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="RankingServlet" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            RANKING
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                    </li>
                     <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                </ul>

            </div>
        </nav>

     <style>
             .cp_link {
	padding: 0.1em 0.3em;
	background-image: linear-gradient(to right, rgba(0,0,0,0) 50%, rgba(255,152,0,1) 50%);
	background-position: 0 0;
	background-size: 200% auto;
	transition: .5s;
	color: #FFFFFF;
}
.cp_link:hover {
	background-position: -100% 0;
	color: #fff;
}


     </style>



        <h1>RANKING</h1>


                <p><a href="FlyWeightServlet"  class="cp_link">FlyWeight</a></p>

                <p><a href="BantamWeight"  class="cp_link">BantamWeight</a></p>

                <p><a href="FeatherWeightServlet"  class="cp_link">FeaterWeight</a></p>

                <p><a href="LightWeightServlet"  class="cp_link">LightWeight</a></p>

                <p><a href="WelterWeightServlet"  class="cp_link">WelterWeight</a></p>

                <p><a href="MiddleWeightServlet"  class="cp_link">MiddleWeight</a></p>

                <p><a href="LightHeavyWeightServlet"  class="cp_link">LightHeavyWeight</a></p>

                <p><a href="HeavyWeightServlet"  class="cp_link">HeavyWeight</a></p>

</body>

</html>
