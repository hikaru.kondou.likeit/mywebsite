<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

</head>

<body background="img/GettyImages-1181916538-728x485.jpg">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="TopPageServlet">UFC</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="NewsServlet">NEWS</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="RankingServlet"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            RANKING
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                    </li>
                     <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                </ul>

            </div>
        </nav>
         <style>





        </style>

        <h1>${frdb.result_title}</h1>
        <p style="color: azure">${frdb.sub_title}</p>
        <div class="row resultImg-area">
                <img src="img/${frdb.file_pass}" width="100%" height="90%">
        </div>


        <div style="background-color: azure">
            <div class="row">
                <div class="col-6">
                    <p style="color: black">イベント名</p>
                </div>
                <div class="col-6">
                    <p style="color: black">${frdb.result_title}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <p style="color: black">開催日</p>
                </div>
                <div class="col-6">
                    <p style="color: black">${frdb.date_time}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <p style="color: black">開催地</p>
                </div>
                <div class="col-6">
                    <p style="color: black">${frdb.venue}</p>
                </div>
            </div>

		<c:forEach var="rdb" items="${rdList}">
            <div style="color: red">
                <p style="text-align: center">${rdb.weightClassDateBeans.weight_class_name}マッチ</p>
            </div>

            <div class="row fight-crad">
                <div class="col-3">
                    <img src="img/${rdb.fighterDateBeans.file_pass}">
                </div>
                <div class="col-3">
                    <p style="color: black">WINNER<br>${rdb.fighterDateBeans.name}<br>戦績　${rdb.fighterDateBeans.win}-${rdb.fighterDateBeans.lose}-${rdb.fighterDateBeans.draw}<br>身長　${rdb.fighterDateBeans.height}<br>体重　${rdb.fighterDateBeans.weight}<br>リーチ　${rdb.fighterDateBeans.reach}</p>
                </div>
                <div class="col-3">
                <p style="color: black"><br>${rdb.fighterDateBeansd.name}<br>戦績　${rdb.fighterDateBeansd.win}-${rdb.fighterDateBeansd.lose}-${rdb.fighterDateBeansd.draw}<br>身長　${rdb.fighterDateBeansd.height}<br>体重　${rdb.fighterDateBeansd.weight}<br>リーチ　${rdb.fighterDateBeansd.reach}</p>
                </div>
                <div class="col-3">
                    <img src="img/${rdb.fighterDateBeansd.file_pass}">
                </div>
            </div>
		</c:forEach>
          <!--    <div style="color: red">
                <p style="text-align: center">${rd.weight_class_id}マッチ</p>
            </div>

            <div class="row fight-crad">
                <div class="col-3">
                    <img src="img/RUA_MAURICIO_L-PRINT.png">
                </div>
                <div class="col-3">
                    <p style="color: black"><br>マウリシオ・フア<br>戦績　26-11-0<br>KO/TKO 80%<br>身長　185cm<br>体重　93kg<br>リーチ　76.00</p>
                </div>
                <div class="col-3">
                    <p style="color: black"><br>ポール・クレイグ<br>戦績　11-3-0<br>KO/TKO 9%<br>身長　192cm<br>体重　93kg<br>リーチ 76.00</p>
                </div>
                <div class="col-3">
                    <img src="img/CRAIG_PAUL_R-PRINT.png">
                </div>
            </div> -->


        </div>

    </div>
</body>

</html>
