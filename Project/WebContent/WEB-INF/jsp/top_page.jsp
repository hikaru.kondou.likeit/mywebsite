<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>top_page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link rel="stylesheet" href="style.css">

</head>

<body class="a" background="img/GettyImages-1181916538-728x485.jpg">
	<div class="container">

		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="TopPageServlet">UFC</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active"><a class="nav-link"
							href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item"><a class="nav-link" href="NewsServlet">NEWS</a>
						</li>
						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle" href="RankingServlet"
							id="navbarDropdown" role="button" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"> RANKING </a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Action</a> <a
									class="dropdown-item" href="#">Another action</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Something else here</a>
							</div>
					     </li>
						<li class="nav-item"><a class="nav-link" href="PhotoServlet"
							tabindex="-1" aria-disabled="true">PHOTO</a>
						</li>
				    <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
					</ul>

				</div>
			</nav>
		</header>

		<div style="margin: 10px;">
			<div class="img col-6 mx-auto">
				<img src="img/${pdb.file_pass}">
				<p class="pp">${pdb.file_comment}</p>
			</div>
		</div>

		<table class="table table-dark">
			<thead>
				<tr>
					<th scope="col">RANKING</th>
					<th scope="col">NAME</th>
					<th scope="col">フライ級</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="fdb" items="${fList}">
					<tr>
						<th scope="row">c</th>
						<td>${fdb.name}</td>
						<td class=”table-img”><img src="img/${fdb.file_pass}">
						</td>
					</tr>
				</c:forEach>
				<!--       <tr>
                    <th scope="row">1</th>
                    <td>ジョセフ・べナビデス</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>ジュシー・フォルミーガ</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>アレクサンドル・パントーハ</td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td>セルジオ・ぺティス</td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td>ティム・エリオット</td>
                </tr>
            </tbody>
            -->
		</table>

		<h2><a href="NewsServlet" class="text-light">新着ニュース</a></h2>

		<div class="row">
			<c:forEach var="ndb" items="${nList}">
				<div class="col-4">
					<img src="img/${ndb.file_pass}" width="100%">
					<p>
						<a href="NewsDetailServlet?id=${ndb.id}" class="text-light">${ndb.news_title}</a>
					</p>
				</div>
			</c:forEach>
			<!-- <div class="col-4">
                <img src="img/GettyImages-1188114166-331x226.jpg">
                <p><a href="news.html" class="text-light">UFCファイトナイト・サンパウロ：<br>フアとクレイグの一戦はスプリットドロー</a></p>
            </div>
            <div class="col-4">
                <img src="img/GettyImages-1188101815-319x226.jpg">
                <p><a href="news.html" class="text-light">UFCファイトナイト・サンパウロ：<br>クラウスがモラエスにKO勝利</a></p>
            </div> -->
		</div>

	<!-- 	<div class="row">
			<div class="col-4">
				<img src="img/pesagem32-339x226.jpeg">
				<p>
					<a href="news.html" class="text-light">UFCファイトナイト・サンパウロ：<br>公式計量結果(2019)
					</a>
				</p>
			</div>

			<div class="col-4">
				<img src="img/GettyImages-1152896371-339x226.jpg">
				<p>
					<a href="news.html" class="text-light">2020年1月、ノースカロライナで<br>ドス・サントスとブレイズが激突
					</a>
				</p>
			</div>

			<div class="col-4">
				<img src="img/GettyImages-1139497898-339x226.jpg">
				<p>
					<a href="news.html" class="text-light">UFCファイトナイト・サンパウロ見どころ：<br>ライトヘビー級に転向した<br>ジャカレの新たな挑戦
					</a>
				</p>
			</div>
		</div>   -->


		<h3 class="text-light">
			<a href="PhotoServlet" class="text-light">画像</a>
		</h3>


		<div class="row">
			<c:forEach var="pdb" items="${pList}">
				<div class="col-4">
					<img src="img/${pdb.file_pass}" width="100%">
					<p class="text-light">${pdb.file_comment}</p>
				</div>
			</c:forEach>
		<!-- 	<div class="col-4">
				<img src="img/GettyImages-1188117749-318x226.jpg">
			</div>

			<div class="col-4">
				<img src="img/GettyImages-1188117749-318x226.jpg">
			</div> -->
		</div>

		<!--     <div class="row">
            <div class="col-4">
                <div class="mt-5">
                    <img src="img/GettyImages-1188117749-318x226.jpg">
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/GettyImages-1188117749-318x226.jpg">
                </div>
            </div>

            <div class="col-4">
                <div class="mt-5">
                    <img src="img/GettyImages-1188117749-318x226.jpg">
                </div>
            </div>
        </div> -->

	</div>
</body>

</html>
