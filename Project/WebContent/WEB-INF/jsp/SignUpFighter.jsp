<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>sign up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

</head>
<div class="container">

    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="TopPageServlet">UFC</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="NewsServlet">NEWS</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="RankingServlet" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            RANKING
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                </ul>

            </div>
        </nav>
    </header>

    <body class="a" background="img/GettyImages-1181916538-728x485.jpg">
        <h1 style="font-size: 40px; color: #ff3300;">FIGHTER SIGN UP</h1>
        <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

       <form action="SignUpFighterServlet" enctype="multipart/form-data"  method="post">
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label"><p style="color: red">name</p></label>
                <div class="col-sm-10">
                    <input type="text" name="name">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">hometown</p></label>
                <div class="col-sm-10">
                    <input type="text" name="hometown">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label"><p style="color: red">age</p></label>
                <div class="col-sm-10">
                    <input type="text" name="age">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">height</p></label>
                <div class="col-sm-10">
                    <input type="text" name="height">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label"><p style="color: red">weight</p></label>
                <div class="col-sm-10">
                    <input type="text" name="weight">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">reach</p></label>
                <div class="col-sm-10">
                    <input type="text" name="reach">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label"><p style="color: red">file</p></label>
                <div class="col-sm-10">
                    <input type="file" name="file">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">win</p></label>
                <div class="col-sm-10">
                    <input type="text" name="win">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">lose</p></label>
                <div class="col-sm-10">
                    <input type="text" name="lose">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">draw</p></label>
                <div class="col-sm-10">
                    <input type="text" name="draw">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">biography</p></label>
                <div class="col-sm-10">
                    <input type="text" name="biography">
                </div>
            </div>

            <div class="form-group">
                <label for="exampleSelect1exampleFormControlSelect1">
                    <p style="color: red">weight_class</p>
                </label>
                <select class="form-control" id="exampleFormControlSelect1" name="weight_class">
                <c:forEach var="wdb" items="${wList}">
                    <option value="${wdb.id}">${wdb.weight_class_name}</option>
                 </c:forEach>
                </select>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">rank</p></label>
                <div class="col-sm-10">
                    <input type="text" name="rank">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label"><p style="color: red">KOTKO</p></label>
                <div class="col-sm-10">
                    <input type="text" name="KOTKO">
                </div>
            </div>

        <input type="submit" value="登録">
       </form>

</div>


</body></html>
