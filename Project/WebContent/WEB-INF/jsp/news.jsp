<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>NEWS</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

</head>

<body background="img/GettyImages-1181916538-728x485.jpg">
    <div class="container">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="TopPageServlet">UFC</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="NewsServlet">NEWS</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="RankingServlet" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                RANKING
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                        </li>
                         <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                    </ul>

                </div>
            </nav>
        </header>

        <h1 style="color: red">NEWS</h1>

		<c:forEach var="newsList" items="${ndbd}">
        <div class="row news-area">
            <div class="col-6">
                <img src="img/${newsList.file_pass}">
            </div>
            <div class="col-6">
                <p class="text-dark"><a href="NewsDetailServlet?id=${newsList.id}" class="text-dark"><strong>
                        <font size="5">${newsList.news_title}</font>
                    </strong><br>${newsList.deta_time}</a></p>
            </div>
        </div>
        </c:forEach>


       <!--   <div class="row news-area">
            <div class="col-6">
                <div class="mt-5">
                    <img src="img/${newsList.file_pass}">
                </div>
            </div>
            <div class="col-6">
                <div class="mt-5">
                    <p class="text-dark"><a href="newsdetail.html" class="text-dark"><strong>
                            <font size="5">${newsList.news_title}</font>
                        </strong><br>${newsList.deta_time}</a></p>
                </div>
            </div>
        </div>

        <div class="row news-area">
            <div class="col-6">
                <div class="mt-5">
                    <img src="img/${newsList.file_pass}">
                </div>
            </div>
            <div class="col-6">
                <div class="mt-5">
                    <p class="text-dark"><a href="newsdetail.html" class="text-dark"><strong>
                            <font size="5">${newsList.news_title}</font>
                        </strong><br>${newsList.deta_time}</a></p>
                </div>
            </div>
        </div>

        <div class="row news-area">
            <div class="col-6">
                <div class="mt-5">
                    <img src="img/${newsList.file_pass}">
                </div>
            </div>
            <div class="col-6">
                <div class="mt-5">
                    <p class="text-dark"><a href="newsdetail.html" class="text-dark"><strong>
                            <font size="5">${newsList.news_title}</font>
                        </strong><br>${newsList.deta_time}</a></p>
                </div>
            </div>
        </div>

        <div class="row news-area">
            <div class="col-6">
                <div class="mt-5">
                    <img src="img/${newsList.file_pass}">
                </div>
            </div>
            <div class="col-6">
                <div class="mt-5">
                    <p class="text-dark"><a href="newsdetail.html" class="text-dark"><strong>
                            <font size="5">${newsList.news_title}</font>
                        </strong><br>${newsList.deta_time}</a></p>
                </div>
            </div>
        </div>

    </div> -->


   <!--   <footer>
        <nav aria-label="...">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="前">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link" href="news.html">1</a></li>
                <li class="page-item"><a class="page-link" href="news2.html">2</a></li>
                <li class="page-item"><a class="page-link" href="news3.html">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="news2.html" aria-label="次">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </footer> -->
</body></html>
