<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>playerdetail</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">
</head>

<body class="a" background="img/GettyImages-1181916538-728x485.jpg">
    <div class="container">

        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="TopPageServlet">UFC</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="ResultServlet">RESULT <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="NewsServlet">NEWS</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="RankingServlet" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                RANKING
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="PhotoServlet" tabindex="-1" aria-disabled="true">PHOTO</a>
                        </li>
                         <li class="nav-item">
                        <a class="nav-link" href="SignUpFighterServlet">SIGN UP</a>
                    </li>
                    </ul>

                </div>
            </nav>
        </header>

        <h1>プロフィール</h1>


        <div class="mt-3">
            <img src="img/${fdbd.file_pass}" width="100%">
        </div>


        <div style="background-color: azure">
            <div class="row">
                <div class="col-6">
                    <p class="text-dark">名前</p>
                </div>
                <div class="col-6">
                    <p class="text-dark">${fdbd.name}</p>
                </div>
            </div>

            <div style="background-color: azure">
                <div class="row">
                    <div class="col-6">
                        <p class="text-dark">出身</p>
                    </div>
                    <div class="col-6">
                        <p class="text-dark">${fdbd.hometown}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <p class="text-dark">年齢</p>
                    </div>
                    <div class="col-6">
                        <p class="text-dark">${fdbd.age}歳</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <p class="text-dark">身長</p>
                    </div>
                    <div class="col-6">
                        <p class="text-dark">${fdbd.height}cm</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <p class="text-dark">体重</p>
                    </div>
                    <div class="col-6">
                        <p class="text-dark">${fdbd.weight}kg</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <p class="text-dark">リーチ</p>
                    </div>
                    <div class="col-6">
                        <p class="text-dark">${fdbd.reach}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <p class="text-dark">戦績</p>
                    </div>
                    <div class="col-6">
                        <p class="text-dark">${fdbd.win}-${fdbd.lose}-${fdbd.draw}</p>
                    </div>
                </div>

            </div>
            <p>${fdbd.biography}</p>

        </div>
</body></html>
