package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.FightResultDateBeans;

public class FightResultDao {
	public ArrayList<FightResultDateBeans> getFightResult() {
		Connection conn = null;
		ArrayList<FightResultDateBeans> fightresultList = new ArrayList<FightResultDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fight_result";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FightResultDateBeans frdb = new FightResultDateBeans();
				frdb.setId(rs.getInt("id"));
				frdb.setResult_title(rs.getString("result_title"));
				frdb.setFile_pass(rs.getString("file_pass"));
				frdb.setDate_time(rs.getTimestamp("date_time"));
				frdb.setVenue(rs.getString("venue"));
				frdb.setSub_title(rs.getString("sub_title"));

				fightresultList.add(frdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fightresultList;
	}

	public FightResultDateBeans getdetail(String id) {
		Connection conn = null;


		try {
			conn = DBManager.getConnection();

			String sql = "select * from fight_result where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

				int idd = rs.getInt("id");
				String file_pass = rs.getString("file_pass");
				String result_title = rs.getString("result_title");
				Timestamp date_time = rs.getTimestamp("date_time");
			    String venue = rs.getString("venue");
			    String sub_title = rs.getString("sub_title");

				return new FightResultDateBeans(idd,file_pass,result_title,date_time,venue,sub_title);


		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}

	}

//	public ResultDetail getResult(String id) {
//		Connection conn = null;
//
//
//		try {
//			conn = DBManager.getConnection();
//
//			String sql = "select * from result_detail where result_id = ?";
//
//			PreparedStatement pStmt = conn.prepareStatement(sql);
//			pStmt.setString(1, id);
//			ResultSet rs = pStmt.executeQuery();
//
//			if(!rs.next()) {
//				return null;
//			}
//
//				int idd = rs.getInt("id");
//				int iddd = rs.getInt("fighter_idd");
//				int result_id = rs.getInt("result_id");
//				int fighter_id = rs.getInt("fighter_id");
//				int weight_class_id = rs.getInt("weight_class_id");
//				String victory_or_defeat = rs.getString("victory_or_defeat");
//
//				return new ResultDetail(idd,iddd,result_id,fighter_id,weight_class_id,victory_or_defeat);
//
//
//		}catch (SQLException e) {
//			 e.printStackTrace();
//	            return null;
//
//		}finally {
//			if(conn != null) {
//				try {
//					conn.close();
//				}catch (SQLException e) {
//					 e.printStackTrace();
//	                    return null;
//
//				}
//			}
//		}
//
//	}

}
