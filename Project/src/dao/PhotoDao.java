package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.PhotoDateBeans;

public class PhotoDao {
	public ArrayList<PhotoDateBeans> getphto() {
		Connection conn = null;
		ArrayList<PhotoDateBeans> photoList = new ArrayList<PhotoDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from photo where id != 13";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				PhotoDateBeans pdb = new PhotoDateBeans();
				pdb.setId(rs.getInt("id"));
				pdb.setFile_pass(rs.getString("file_pass"));
				pdb.setFile_comment(rs.getString("file_comment"));

				photoList.add(pdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return photoList;
	}

	public PhotoDateBeans getPhoto() {
		Connection conn = null;


		try {
			conn = DBManager.getConnection();

			String sql = "select * from photo where id =13";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			if(!rs.next()) {
				return null;
			}

				int id = rs.getInt("id");
				String file_pass = rs.getString("file_pass");
				String file_comment = rs.getString("file_comment");

				return new PhotoDateBeans(id,file_pass,file_comment);


		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}

	}

}
