package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ResultDetail;

public class ResultDetailDao {
	public ArrayList<ResultDetail> getFightResult(String id) {
		Connection conn = null;
		ArrayList<ResultDetail> resultList = new ArrayList<ResultDetail>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from result_detail where result_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();


			while(rs.next()) {
				ResultDetail rd = new ResultDetail();
				rd.setId(rs.getInt("id"));
				rd.setFighter_id(rs.getInt("fighter_id"));
				rd.setFighter_idd(rs.getInt("fighter_idd"));
				rd.setResult_id(rs.getInt("result_id"));
				rd.setWeight_class_id(rs.getInt("weight_class_id"));
				rd.setVictory_or_defeat(rs.getString("victory_or_defeat"));

				resultList.add(rd);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return resultList;
	}

}
