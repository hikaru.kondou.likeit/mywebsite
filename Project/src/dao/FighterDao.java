package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.FighterDateBeans;

public class FighterDao {
	public ArrayList<FighterDateBeans> getFlyWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class = '1' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}

	public ArrayList<FighterDateBeans> getBantamWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class ='1' and rank = '1' or weight_class ='2' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}

	public ArrayList<FighterDateBeans> getFeatherWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class = '3' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}

	public ArrayList<FighterDateBeans> getLightWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class = '4' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}

	public ArrayList<FighterDateBeans> geMiddleWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class = '6' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}


	public ArrayList<FighterDateBeans> geWelterWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class = '5' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}


	public ArrayList<FighterDateBeans> geLightHeavyWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class = '7' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}

	public ArrayList<FighterDateBeans> getHeavyWeight() {
		Connection conn = null;
		ArrayList<FighterDateBeans> fighterList = new ArrayList<FighterDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where weight_class ='7' and rank = '3' or weight_class = '8' order by rank asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				FighterDateBeans fdb = new FighterDateBeans();
				fdb.setId(rs.getInt("id"));
				fdb.setName(rs.getString("name"));
				fdb.setHometown(rs.getString("hometown"));
				fdb.setAge(rs.getInt("age"));
				fdb.setHeight(rs.getInt("height"));
				fdb.setWeight(rs.getInt("weight"));
				fdb.setReach(rs.getInt("reach"));
				fdb.setFile_pass(rs.getString("file_name"));
				fdb.setWin(rs.getInt("win"));
				fdb.setLose(rs.getInt("lose"));
				fdb.setDraw(rs.getInt("draw"));
				fdb.setBiography(rs.getString("biography"));
				fdb.setWeight_class(rs.getString("weight_class"));
				fdb.setRank(rs.getInt("rank"));
				fighterList.add(fdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return fighterList;
	}

	public FighterDateBeans getFighterDetail(String id) {
		Connection conn = null;


		try {
			conn = DBManager.getConnection();

			String sql = "select * from fighter where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			 int idd = rs.getInt("id");
			 String name = rs.getString("name");
			 String hometown = rs.getString("hometown");
			 int age = rs.getInt("age");
			 int height = rs.getInt("height");
			 int weight = rs.getInt("weight");
			 int reach = rs.getInt("reach");
			 String file_pass = rs.getString("file_name");
		     int win = rs.getInt("win");
			 int lose = rs.getInt("lose");
			 int draw = rs.getInt("draw");
			 String biography = rs.getString("biography");
			 String weight_class = rs.getString("weight_class");
			 int rank = rs.getInt("rank");

			 return new FighterDateBeans(idd,name,hometown,age,height,weight,reach,file_pass,win,lose,draw,biography,weight_class,rank);

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
	}

	public void SignUpFighter(String name, String hometown, int age, int height, int weight, int reach, String file_name, int win, int lose, int draw, String biography, String weight_class, int rank, int KOTKO ) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO fighter(name,hometown,age,height,weight,reach,file_name,win,lose,draw,biography,weight_class,rank,KOTKO) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, hometown);
			pStmt.setInt(3, age);
			pStmt.setInt(4, height);
			pStmt.setInt(5, weight);
			pStmt.setInt(6, reach);
			pStmt.setString(7,file_name );
			pStmt.setInt(8, win);
			pStmt.setInt(9, lose);
			pStmt.setInt(10, draw);
			pStmt.setString(11, biography);
			pStmt.setString(12,weight_class);
			pStmt.setInt(13, rank);
			pStmt.setInt(14, KOTKO);
			pStmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}




}


