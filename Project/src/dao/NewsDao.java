package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.NewsDateBeans;

public class NewsDao {
	public ArrayList<NewsDateBeans> getNews() {
		Connection conn = null;
		ArrayList<NewsDateBeans> newsList = new ArrayList<NewsDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "select * from news where id !=6";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				NewsDateBeans ndb = new NewsDateBeans();
				ndb.setId(rs.getInt("id"));
				ndb.setNews_title(rs.getString("news_title"));
				ndb.setDeta_time(rs.getTimestamp("date_time"));
				ndb.setFile_pass(rs.getString("file_pass"));
				ndb.setDetail(rs.getString("detail"));

				newsList.add(ndb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return newsList;
	}

	public NewsDateBeans getdetail(String id) {
		Connection conn = null;


		try {
			conn = DBManager.getConnection();

			String sql = "select * from news where id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

				int idd = rs.getInt("id");
				String news_title = rs.getString("news_title");
				Timestamp date_time = rs.getTimestamp("date_time");
				String file_pass = rs.getString("file_pass");
			    String detail = rs.getString("detail");

				return new NewsDateBeans(idd,news_title,date_time,file_pass,detail);


		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}

	}


		public ArrayList<NewsDateBeans> GetNews() {
			Connection conn = null;
			ArrayList<NewsDateBeans> newsList = new ArrayList<NewsDateBeans>();

			try {
				conn = DBManager.getConnection();

				String sql = "select * from news";

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while(rs.next()) {
					NewsDateBeans ndb = new NewsDateBeans();
					ndb.setId(rs.getInt("id"));
					ndb.setNews_title(rs.getString("news_title"));
					ndb.setDeta_time(rs.getTimestamp("date_time"));
					ndb.setFile_pass(rs.getString("file_pass"));
					ndb.setDetail(rs.getString("detail"));

					newsList.add(ndb);
				}

			}catch (SQLException e) {
				 e.printStackTrace();
		            return null;

			}finally {
				if(conn != null) {
					try {
						conn.close();
					}catch (SQLException e) {
						 e.printStackTrace();
		                    return null;

					}
				}
			}
			return newsList;
		}

//	public ArrayList<NewsDateBeans> getdetail(String id) {
//		Connection conn = null;
//		ArrayList<NewsDateBeans> newsList = new ArrayList<NewsDateBeans>();
//
//		try {
//			conn = DBManager.getConnection();
//
//			String sql = "select * from news where id = ?";
//
//			PreparedStatement pStmt = conn.prepareStatement(sql);
//			pStmt.setString(1, id);
//			ResultSet rs = pStmt.executeQuery();
//
//
//			while(rs.next()) {
//				NewsDateBeans ndb = new NewsDateBeans();
//				ndb.setId(rs.getInt("id"));
//				ndb.setNews_title(rs.getString("news_title"));
//				ndb.setDeta_time(rs.getTimestamp("date_time"));
//				ndb.setFile_pass(rs.getString("file_pass"));
//				ndb.setDetail(rs.getString("detail"));
//
//				newsList.add(ndb);
//			}
//
//		}catch (SQLException e) {
//			 e.printStackTrace();
//	            return null;
//
//		}finally {
//			if(conn != null) {
//				try {
//					conn.close();
//				}catch (SQLException e) {
//					 e.printStackTrace();
//	                    return null;
//
//				}
//			}
//		}
//		return newsList;
//	}





}
