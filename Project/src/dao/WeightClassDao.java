package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.WeightClassDateBeans;

public class WeightClassDao {
	public WeightClassDateBeans getweight(String id) {
		Connection conn = null;


		try {
			conn = DBManager.getConnection();

			String sql = "select * from weight_class where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

				int idd = rs.getInt("id");
				String weight_class_name = rs.getString("weight_class_name");

				return new WeightClassDateBeans(idd,weight_class_name);


		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}

	}

	public ArrayList<WeightClassDateBeans> getWeight() {
		Connection conn = null;
		ArrayList<WeightClassDateBeans> weightList = new ArrayList<WeightClassDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM weight_class";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				WeightClassDateBeans wdb = new WeightClassDateBeans();
				wdb.setId(rs.getInt("id"));
				wdb.setWeight_class_name(rs.getString("weight_class_name"));
				weightList.add(wdb);
			}

		}catch (SQLException e) {
			 e.printStackTrace();
	            return null;

		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch (SQLException e) {
					 e.printStackTrace();
	                    return null;

				}
			}
		}
		return weightList;
	}

}
