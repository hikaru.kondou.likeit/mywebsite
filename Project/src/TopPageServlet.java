

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.FighterDateBeans;
import beans.NewsDateBeans;
import beans.PhotoDateBeans;
import dao.FighterDao;
import dao.NewsDao;
import dao.PhotoDao;

/**
 * Servlet implementation class TopPageServlet
 */
@WebServlet("/TopPageServlet")
public class TopPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		PhotoDao pdo = new PhotoDao();
		PhotoDateBeans pdb = new PhotoDateBeans ();
		pdb = pdo.getPhoto();

		request.setAttribute("pdb", pdb);

		FighterDao fdoa = new FighterDao();
		ArrayList<FighterDateBeans> fList = new ArrayList<FighterDateBeans>();
		fList = fdoa.getFlyWeight();

		request.setAttribute("fList", fList);

		NewsDao ndao = new NewsDao();
		ArrayList<NewsDateBeans> nList = new ArrayList<NewsDateBeans>();
		nList = ndao.GetNews();



		request.setAttribute("nList", nList);

		PhotoDao pdoa = new PhotoDao();
		ArrayList<PhotoDateBeans> pList = new ArrayList<PhotoDateBeans>();
		pList = pdoa.getphto();

		request.setAttribute("pList", pList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top_page.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
