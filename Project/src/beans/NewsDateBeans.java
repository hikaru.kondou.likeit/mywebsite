package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class NewsDateBeans implements Serializable {
	private int id;
	private String news_title;
	private Timestamp deta_time;
	private String file_pass;
	private String detail;

	public NewsDateBeans() {

	}

	public NewsDateBeans(int id, String news_title, Timestamp dete_time, String file_pass, String detail) {
		this.id = id;
		this.news_title = news_title;
		this.deta_time = deta_time;
		this.file_pass= file_pass;
		this.detail = detail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNews_title() {
		return news_title;
	}

	public void setNews_title(String news_title) {
		this.news_title = news_title;
	}

	public Timestamp getDeta_time() {
		return deta_time;
	}

	public void setDeta_time(Timestamp deta_time) {
		this.deta_time = deta_time;
	}

	public String getFile_pass() {
		return file_pass;
	}

	public void setFile_pass(String file_pass) {
		this.file_pass = file_pass;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
