package beans;

import java.io.Serializable;
import java.sql.Timestamp;



public class FightResultDateBeans implements Serializable {
	private int id;
	private String file_pass;
	private String result_title;
	private Timestamp date_time;
	private String venue;
	private String sub_title;

	public FightResultDateBeans(int id,String file_pass ,String result_title, Timestamp date_time, String venue, String sub_title) {
		this.id = id;
		this.file_pass = file_pass;
		this.result_title = result_title;
		this.date_time = date_time;
		this.venue = venue;
		this.sub_title = sub_title;
	}

	public FightResultDateBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFile_pass() {
		return file_pass;
	}

	public void setFile_pass(String file_pass) {
		this.file_pass = file_pass;
	}

	public String getResult_title() {
		return result_title;
	}

	public void setResult_title(String result_title) {
		this.result_title = result_title;
	}

	public Timestamp getDate_time() {
		return date_time;
	}

	public void setDate_time(Timestamp date_time) {
		this.date_time = date_time;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getSub_title() {
		return sub_title;
	}

	public void setSub_title(String sub_title) {
		this.sub_title = sub_title;
	}



}
