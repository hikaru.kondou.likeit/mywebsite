package beans;

import java.io.Serializable;

public class FighterDateBeans implements Serializable {
	private int id;
	private String name;
	private String hometown;
	private int age;
	private int height;
	private int weight;
	private int reach;
	private String file_pass;
	private int win;
	private int lose;
	private int draw;
	private String biography;
	private String weight_class;
	private int rank;
	private int KOTKO;


	public FighterDateBeans(int id, String name, String hometown, int age, int height, int weight, int reach,
			String file_pass, int win, int lose, int draw, String biography, String weight_class, int rank) {
		this.id =id;
		this.name = name;
		this.hometown = hometown;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.reach = reach;
		this.file_pass = file_pass;
		this.win = win;
		this.lose = lose;
		this.draw = draw;
		this.biography = biography;
		this.weight_class = weight_class;
		this.rank = rank;
	}

	public FighterDateBeans(String name, String hometown, int age, int height, int weight, int reach,
			String file_pass, int win, int lose, int draw, String biography, String weight_class, int rank, int KOTKO) {
		this.name = name;
		this.hometown = hometown;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.reach = reach;
		this.file_pass = file_pass;
		this.win = win;
		this.lose = lose;
		this.draw = draw;
		this.biography = biography;
		this.weight_class = weight_class;
		this.rank = rank;
		this.KOTKO = KOTKO;
	}

	public int getKOTKO() {
		return KOTKO;
	}

	public void setKOTKO(int kOTKO) {
		KOTKO = kOTKO;
	}


	public FighterDateBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getReach() {
		return reach;
	}

	public void setReach(int reach) {
		this.reach = reach;
	}

	public String getFile_pass() {
		return file_pass;
	}

	public void setFile_pass(String file_pass) {
		this.file_pass = file_pass;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}

	public int getLose() {
		return lose;
	}

	public void setLose(int lose) {
		this.lose = lose;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getWeight_class() {
		return weight_class;
	}

	public void setWeight_class(String weight_class) {
		this.weight_class = weight_class;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

}
