package beans;

import java.io.Serializable;

public class WeightClassDateBeans implements Serializable {
	private int id;
	private String weight_class_name;

	public WeightClassDateBeans(int id, String weight_class_name) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.weight_class_name = weight_class_name;
	}

	public WeightClassDateBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWeight_class_name() {
		return weight_class_name;
	}

	public void setWeight_class_name(String weight_class_name) {
		this.weight_class_name = weight_class_name;
	}

}
