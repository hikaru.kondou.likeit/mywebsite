package beans;

import java.io.Serializable;

public class PhotoDateBeans implements Serializable {
	private int id;
	private String file_pass;
	private String file_comment;

	public PhotoDateBeans(int id, String file_pass, String file_comment) {
		this.id = id;
		this.file_pass = file_pass;
		this.file_comment = file_comment;

	}

	public PhotoDateBeans() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFile_pass() {
		return file_pass;
	}

	public void setFile_pass(String file_pass) {
		this.file_pass = file_pass;
	}

	public String getFile_comment() {
		return file_comment;
	}

	public void setFile_comment(String file_comment) {
		this.file_comment = file_comment;
	}

}
