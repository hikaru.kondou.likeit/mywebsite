package beans;

import java.io.Serializable;

import dao.FighterDao;
import dao.WeightClassDao;

public class ResultDetail implements Serializable {
	private int id;
	private int fighter_idd;
	private int result_id;
	private int fighter_id;
	private int weight_class_id;
	private String victory_or_defeat;

	public ResultDetail(int id, int idd, int result_id, int fighter_id, int weight_class_id,
			String victory_or_defeat) {
		this.id = id;
		this.id = idd;
		this.result_id = result_id;
		this.fighter_id = fighter_id;
		this.weight_class_id = weight_class_id;
		this.victory_or_defeat = victory_or_defeat;



	}

	public ResultDetail() {
		// TODO 自動生成されたコンストラクター・スタブ
	}



	public int getFighter_idd() {
		return fighter_idd;
	}

	public void setFighter_idd(int fighter_idd) {
		this.fighter_idd = fighter_idd;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getResult_id() {
		return result_id;
	}

	public void setResult_id(int result_id) {
		this.result_id = result_id;
	}

	public int getFighter_id() {
		return fighter_id;
	}

	public void setFighter_id(int fighter_id) {
		this.fighter_id = fighter_id;
	}

	public int getWeight_class_id() {
		return weight_class_id;
	}

	public void setWeight_class_id(int weight_class_id) {
		this.weight_class_id = weight_class_id;
	}

	public String getVictory_or_defeat() {
		return victory_or_defeat;
	}

	public void setVictory_or_defeat(String victory_or_defeat) {
		this.victory_or_defeat = victory_or_defeat;
	}

	// 結合テーブル情報
	public FighterDateBeans getFighterDateBeans() {
		FighterDao dao = new FighterDao();
		return dao.getFighterDetail(String.valueOf(this.fighter_id));

	}

	public FighterDateBeans getFighterDateBeansd() {
		FighterDao dao = new FighterDao();
		return dao.getFighterDetail(String.valueOf(this.fighter_idd));

	}

	public WeightClassDateBeans getWeightClassDateBeans() {
		WeightClassDao wdo = new WeightClassDao();
		return wdo.getweight(String.valueOf(this.weight_class_id));
	}





}
