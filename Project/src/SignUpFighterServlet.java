

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.WeightClassDateBeans;
import dao.FighterDao;
import dao.WeightClassDao;

/**
 * Servlet implementation class SignUpFighterServlet
 */
@WebServlet("/SignUpFighterServlet")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-NU87N8R.000.001\\Documents\\mywebsite\\Project\\WebContent\\img", maxFileSize=1048576)
public class SignUpFighterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpFighterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		WeightClassDao wdao = new WeightClassDao();
		ArrayList<WeightClassDateBeans> wList = new ArrayList<WeightClassDateBeans>();
		wList = wdao.getWeight();

		request.setAttribute("wList", wList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUpFighter.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");

		String hometown = request.getParameter("hometown");

		String age = request.getParameter("age");
		int ag = Integer.parseInt(age);

		String height = request.getParameter("height");
		int he = Integer.parseInt(height);

		String weight = request.getParameter("weight");
		int we = Integer.parseInt(weight);

		String reach = request.getParameter("reach");
		int re = Integer.parseInt(reach);

		Part file = request.getPart("file");
		 String fname = this.getFileName(file);
		file.write(fname);

		String win = request.getParameter("win");
		int wi = Integer.parseInt(win);

		String lose = request.getParameter("lose");
		int ls = Integer.parseInt(lose);

		String draw = request.getParameter("draw");
		int dr = Integer.parseInt(draw);

		String biography = request.getParameter("biography");

		String weight_class = request.getParameter("weight_class");

		String rank = request.getParameter("rank");
		int ra = Integer.parseInt(rank);

		String KOTKO = request.getParameter("KOTKO");
		int ko = Integer.parseInt(KOTKO);

//		if(name.equals("") || file_name.equals("")) {
//			request.setAttribute("errMsg", "入力された内容は正しくありません。");
//
//			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUpFighter.jsp");
//			dispatcher.forward(request, response);
//		}



		FighterDao fdao = new FighterDao();
		fdao.SignUpFighter(name, hometown, ag, he, we, re, fname, wi, ls, dr, biography, weight_class, ra, ko);

		response.sendRedirect("TopPageServlet");



	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
