

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.FighterDateBeans;
import dao.FighterDao;

/**
 * Servlet implementation class BantamWeight
 */
@WebServlet("/BantamWeight")
public class BantamWeightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BantamWeightServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		FighterDao feighterdao = new FighterDao();
		ArrayList<FighterDateBeans> fdbList = new ArrayList<FighterDateBeans>();
		fdbList = feighterdao.getBantamWeight();

		request.setAttribute("fdbList", fdbList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BantamWeight.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
