

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.FightResultDateBeans;
import beans.ResultDetail;
import dao.FightResultDao;
import dao.ResultDetailDao;

/**
 * Servlet implementation class ResultDetailServlet
 */
@WebServlet("/ResultDetailServlet")
public class ResultDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResultDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		FightResultDao fdao =new FightResultDao();
		FightResultDateBeans frdb = new FightResultDateBeans();
		frdb = fdao.getdetail(id);

		request.setAttribute("frdb", frdb);

		ResultDetailDao fdo =new ResultDetailDao();
		ArrayList<ResultDetail> rdList = new ArrayList<ResultDetail>();
		rdList = fdo.getFightResult(id);

		request.setAttribute("rdList", rdList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/resultdetail.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
